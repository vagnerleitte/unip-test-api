function item(connection) {
  this._connection = connection;
}

item.prototype.list = function(callback) {
  this._connection.query("SELECT * FROM items", callback);
};

item.prototype.findById = function(id, callback) {
  this._connection.query("SELECT * FROM items where id = ?", [id], callback);
};

item.prototype.delete = function(id, callback) {
  this._connection.query("DELETE FROM items where id = ?", [id], callback);
};

item.prototype.create = function(item, callback) {
  this._connection.query("INSERT INTO items SET ?", item, callback);
};

item.prototype.update = function(item, id, callback) {
  var fields = "";
  
  Object.keys(item).forEach(field => {
      fields += `${field} = ?, `;
  });

  fields = fields.substring(0, fields.length - 2);

  this._connection.query(
    `UPDATE items SET ${fields} where id = ?`,
    [...Object.values(item), id],
    callback
  );
};

module.exports = function() {
  return item;
};
