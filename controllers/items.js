const { check, validationResult } = require("express-validator");

module.exports = function(app) {
  app.get("/items", function(req, res) {
    const dbConnection = app.database.connectionFactory;
    var itemsDAO = new app.dao.item(dbConnection);
    itemsDAO.list((error, result) => {
      if (error) {
        res.status(500).json(error);
      }
      res.json({items: result});
    });
  });

  app.get("/item/:id", function(req, res) {
    const dbConnection = app.database.connectionFactory;
    var itemsDAO = new app.dao.item(dbConnection);

    itemsDAO.findById(req.params.id, function(error, result) {
      if (error) {
        return status(500).json(error);
      }

      res.json(result);
    });
  });

  app.post(
    "/item",
    [
      // username must be an email
      check("item.title").exists()
    ],
    function(req, res) {
      const dbConnection = app.database.connectionFactory;
      var itemsDAO = new app.dao.item(dbConnection);

      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      itemsDAO.create(req.body.item, function(error) {
        if (error) {
          res.status(500).json(error);
        }

        res.status(201).json();
      });
    }
  );

  app.put(
    "/item/:id",
    function(req, res) {
      const dbConnection = app.database.connectionFactory;
      var itemsDAO = new app.dao.item(dbConnection);

      itemsDAO.update(req.body.item, req.params.id, function(error) {
        if (error) {
          res.status(500).json(error);
        }

        res.status(200).json();
      });
    }
  );

  app.delete("/item/:id", function(req, res) {
    const dbConnection = app.database.connectionFactory;
    var itemsDAO = new app.dao.item(dbConnection);

    itemsDAO.delete(req.params.id, function(error) {
      if (error) {
        res.status(500).json(error);
      }
      res.status(204).json();
    });
    
  });
};
