var express = require("express");
var consign = require("consign");
var bodyParser = require("body-parser");
var cors = require("cors");

var app = express();

app.use(bodyParser.json());
app.use(cors());

consign()
  .include("controllers")
  .then("database")
  .then('dao')
  .into(app);

module.exports = app;
