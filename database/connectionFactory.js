var mysql = require('mysql');

function createDbConnection() {
    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'ROOT@admin36',
        database: 'menumanager'
    });
}

module.exports = createDbConnection;