var app = require("./config/express");
const normalizePort = require('normalize-port');
const port = normalizePort(process.env.PORT || '3000');

app.listen(port, function() {
    console.log(`Server running on port ${port}.`);
});